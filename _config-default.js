

// Set the NODE_ENV when calling your gulp task
// export NODE_ENV=production
var prodLikeEnvs = ['production', 'staging'];
var fs = require("fs");

module.exports = {
  // File system
  BasePath: _BASE_PATH,
  SrcPath: _BASE_PATH + _SRC_DIR,
  BuildPath: _BASE_PATH + _BUILD_DIR,
  sassPath: _SRC_PATH + _SASS_DIR,
  cssPath: _SRC_PATH + _CSS_DIR,
  ImagePath: _SRC_PATH + _IMG_DIR,
  JsPath: _SRC_PATH + _JS_DIR,
  // Plugins options
  sass: { // https://github.com/sass/node-sass#options
    errLogToConsole: true,
    outputStyle: 'compact', // nested, expanded, compact, compressed
    includePaths: []
  },
  autoprefixer:{ // https://github.com/postcss/autoprefixer#options
      overrideBrowserslist: ['last 2 versions'],
      cascade: false
  },
  // https://github.com/postcss/postcss-url
  postcssUrl: {
    url: 'inline',
    // Relative to source file
    basePath: '../images/'
  },
  // @unused
  mqpacker: {
    sort: true
  },
  // @test
  doiuse: {
    browsers:['ie >= 11'],
    ignore: ['rem'], // an optional array of features to ignore
    ignoreFiles: ['**/normalize.css'], // an optional array of file globs to match against original source file path, to ignore
    onFeatureUsage: function(usageInfo) {
      console.log(usageInfo.message);
    }
  },
  sourcemaps:{
      path: './',
      sourceRoot: '../_scss'
  },
  browserSync: {
       server: {
           baseDir: _BUILD_PATH
       }
  },
  fontblast:{
    sourceFont: _SRC_PATH + 'assets/fonts/icons/font/icons.svg',
    glyph: _SRC_PATH + 'assets/fonts/icons/glyph.scss',
    destPath: _BUILD_PATH + 'fontblast/',
    cleanAfter: false
  },
  // https://github.com/shakyShane/gulp-svg-sprites
  svgSprite: {
    sourcesPath: _SRC_PATH + 'assets/svg/',
    spriteDest: _BASE_PATH ,
    svgoPlugins: {
        plugins: [ // https://github.com/svg/svgo#what-it-can-do
            {
              removeViewBox: false
            },
            {
              removeStyleElement: true
            },
            {
              removeTitle: false
            },
            {
              cleanupIDs: false
            }
        ]
    },
    options: {
    // https://www.npmjs.com/package/gulp-svg-sprites#options
    // https://github.com/shakyShane/gulp-svg-sprites#using-the-built-in-scss-template
      mode: "sprite", // sprite|defs|symbols
      common: "icon",
      selector: "icon-%f",
      layout: "vertical", // horizontal|diagonal
      svgId: "%f",
      // Only in sprite mode
      cssFile: _SRC_DIR + _SASS_DIR + "_sprite.scss",
      baseSize: 100,
      // Path in preview files and css generated classes
      // @bug - %f is not replaced in symbol mode
      svgPath:  "images/sprite.svg",
      templates: {
        scss: fs.readFileSync("gulp-tasks/tmpl/sprite.scss", "utf-8"),
        previewSprite: fs.readFileSync("gulp-tasks/tmpl/sprite_preview.njk", "utf-8"),
        previewSymbols: fs.readFileSync("gulp-tasks/tmpl/symbols_preview.njk", "utf-8")
      },
      // Custom files names & path relative to spriteDest
      svg: {
        sprite: _SRC_DIR + "assets/images/sprite.svg",
        symbols: _SRC_DIR + "assets/images/symbols.svg"
      },
      preview: { // or false
        css: false,
        sprite: _SRC_DIR + "templates/sprite_demo.njk",
        symbols: _SRC_DIR + "templates/symbols_demo.njk"
      }
    }
  },
  // https://marked.js.org/#/USING_ADVANCED.md
  markedConfig: {
    // highlight: function (code) {
    //     return require('highlight.js').highlightAuto(code).value;
    // },
      baseUrl: null, // A prefix url for any relative link
      gfm: true, // Enable GitHub flavored markdown.
      tables: true, // Enable GFM tables. This option requires the gfm option to be true.
      breaks: false, // Enable GFM line breaks. This option requires the gfm option to be true.
      pedantic: false,
      sanitize: false, // Sanitize the output. Ignore any HTML that has been input.
      smartLists: true,
      smartypants: false // Use "smart" typograhic punctuation for things like quotes and dashes.
  },
  frontmatterConfig: {
      language:'yaml',
      delimiters:'---' // Default
  },
  nunjuks:{
      defaultTemplatesPath: _SRC_PATH + _TEMPLATES_DIR,
      searchPaths: [
        _SRC_PATH + _PROTOTYPES_DIR,
        _SRC_PATH + _TEMPLATES_DIR,
        _SRC_PATH + _IMG_DIR
      ],
      templateExt: '.njk',
      options: {
        autoescape:false,
        throwOnUndefined: false
      }
  },
  // Emailing - MJML
  mjml:{
    env:{
      // 
    },
    options: {
      keepComments: true,
      beautify: false,
      minify: false,
      validationLevel: 'soft',
      filePath: '.'
    }
  },
  // https://github.com/kangax/html-minifier#options-quick-reference
  htmlmin:{
    collapseWhitespace: true
  },
  // https://www.browsersync.io/docs/options
  browserSync: {
    server: {
      baseDir: _BUILD_PATH
    }
  },
  bundleConfig:{
        dest: '',
        // dest: _BASE_PATH + _BUILD_DIR,
        // précéder le path de l'url de prod pour que les chemins soit correct
        // y compris quand on parcour l'arborescence des pages
        // pathPrefix: 'http:localhost:3000/',
        pathPrefix: '/',
        fileName: 'bundle.result'
  },
  bundle: {

  },
  copy: [

  ]
};
