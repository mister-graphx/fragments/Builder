## CHANGELOG

- Ajout du support de .env files par projets
- log + psr 

2.2.3

task newsletter

* utilisation de nunjuks et de variables dans les templates mjml
* variables d'environnement et variables "locale" (spécifique au fichier de template traité)

Chaque fichier template (situé a la racine de /emails) peut être accompagné de son fichier de data.

2.2.2

task Styles : Ajout de postcss-url et postcss-custom-properties

ajout de la config par defaut pour postcss-url

2.2.1

* spécifier le `NODE_ENV` depuis la config `mode: 'production'`, ou depuis la CLI , sinon `production`.

* ajout de gulp-htmlmin
* suivant le mode minifier le html sur les taches jekyll et prototype.
* ajout d'une config.htmlmin surchargeable depuis le projet


v2.2.0

* utilisation de runsequence sur les taches.
* amélioration des temps d'execution des taches.
* suppression des loops du au taches qui s'éxecutaient en synchrone et au watch qui les relançaient


BUGFIX : les taches ne s'enchaine pas correctements sans retour du stream de gulp.
http://macr.ae/article/splitting-gulpfile-multiple-files.html en référence au comment http://disq.us/p/yhucmu
et au snippet
https://pastebin.com/K1Ztr5d3

utilisation de run-sequence pour l'enchainement des tache en mode synchrone ou asynchrone vu que bundle-asset n'est pas porté pour gulp 4.


v2.1.0

passage a yarn abandon de bower
support de modules eyeglass

v2.0.1

- maj des dependances
- [test] ajout de fontblast et d'une tache qui extrait les svg d'une font svg, afin de les réutiliser ensuite avec la tache sprite.
Pour les projets basé sur fragments on récupère en plus la glyph depuis les settings scss $icon-set, afin de pouvoir renommer correctement les fichiers svg générés.

v2.0.0

- remplacement de swig par Nunjuks
- Passage a postCss pour Autoprefixer et les post traitement css après libSass
- refonte de la configuration des projets
- les noms des dossiers de travail, ainsi que les chemins passe en Globales
