# Fragments Builder


## Installation


**Pré-requis : Node (> v6) Yarn recommandé**

```shell
# in your working directory,localhost folder create a folder for the Builder
# ex: _FACTORY
cd ~/Sites/_FACTORY
# Get the folder structure and tasks
git clone https://github.com/mistergraphx/Fragments_builder.git .
# Install required node modules
yarn install
```


## Définir l'environement

le mode par défaut est `production`

```shell
# Changer l'environnement pour les bundles générés
export NODE_ENV=production
export NODE_ENV=development
```

depuis le fichier `app.js` `mode: 'production|development'`.

## Démarrer un nouveau projet

Le projet est le seul argument passé au taches, c'est ce qui défini le chemin ou d'ou sera chargé le fichier app.js permettant de modifier les paramètres par défaut (cf. _config-default.js), définir le bundle, etc…

Créer un dossier et un fichier app.js.

```bash
# Partir du boilerplate
cp -R _KITCHEN/_app-boilerplates/starter _KITCHEN/mon_projet
# Si besoin modifier la config du projet dans app.js
# Lancer la tache par défaut
gulp --project=mon_projet
```


### Structure du projet

* _BUILD/
* _src/
    * 	assets/
        *	_scss/
        *	css/
        *	fonts/
        *	images/
        *	js/
    * 	datas/ : optionnel *Dossier des fichiers `json` optionnel de datas pour les pages*
    *   pages/ : optionnel Fichers markdown de contenu jekill like pages
    *   prototypes/ optionnel : fichiers nunjuks de contenu pour prototyper rapidement du html
    * 	templates/ optionnel : *Dossier des templates*
        *	_layouts/
        *	_partials/
        *	index.njk
* app.js



## Ajout de modules a un projet

```bash

cd _KITCHEN/mon_projet
# initialiser un pakage yarn
yarn init
# Ajouter les dépendances
yarn add fragments@https://gitlab.com/mister-graphx/fragments/framework.git

```

## Taches

### Default

`gulp --project=PROJECT_NAME` : produit les css et le bundle défini dans app.js

`gulp config --project=PROJECT_NAME` - Retourne la config actuelle du projet

### Styles

`gulp styles:watch --project=PROJECT_NAME`

* Compile les scss et traitements Postcss
* Produit le bundle (css,js,copy)

### Jekyll pages

`gulp jekyll:watch --project=PROJECT_NAME`

* Génère les styles et bundle.
* Génère un site static en partant des fichier markdown du dossier `_src/pages/` `global._PAGES_DIR`.


### Prototype

`gulp prototype:watch --project=PROJECT_NAME`

`global._PROTOTYPES_DIR` : `prototypes/`

Génère des pages html dans le dossier `BUILD/prototypes/` à partir des fichiers .njk situés dans `_src/prototypes/` .


### Testing, Linting :


```
# Additionellement installer lighthouse
npm install -g lighthouse
cd _KITCHEN/mon_projet/
lighthouse http://mon_projet.local [--view: ouvre directement dans le navigateur le rapport .]
```



### Svg Utils

[Tache - Build Sprite](gulp-tasks/svgSprite.js)

Usage : `gulp build-sprite --project=project_name`

Création de sprites svg

**Config**

```javascript
svgSprite: {
  sourcesPath: _SRC_PATH + 'assets/svg/',
  spriteDest: _BASE_PATH ,
  svgoPlugins: {
      plugins: [ // https://github.com/svg/svgo#what-it-can-do
          {
            removeViewBox: false
          },
          {
            removeStyleElement: true
          },
          {
            cleanupIDs: false
          }
      ]
  },
  options: { // https://www.npmjs.com/package/gulp-svg-sprites#options
    mode: "symbols", // sprite|defs|symbols
    common: "icon",
    selector: "%f",
    layout: "vertical", // horizontal|diagonal
    svgId: "%f",
    cssFile: _SRC_DIR + _SASS_DIR + "_sprite.scss", // Only in sprite mode
    baseSize: 64,
    templates: {
      scss: require("fs").readFileSync("gulp-tasks/tmpl/sprite.scss", "utf-8"),
    },
    // Custom files names & path relative to spriteDest
    svg: {
      symbols: _BUILD_DIR + "assets/images/symbols.svg"
    },
    preview: { // or false
      symbols: _SRC_DIR + "templates/symbols_demo.twig"
    }
  }
},

```

#### Extraction de fichiers svg depuis une font svg

[Tache - fontblast](gulp-tasks/fontblast.js)

Usage : `gulp font-blast --project=project_name`

Extraction de fichiers svg depuis une font svg (utilitaire de convertion pour passer progressivement aux sprites svg);

**Config**

```javascript
fontblast:{
    // svgFont : font utilisé par FontBlast pour extraire les svg
    sourceFont: _SRC_PATH + 'assets/fonts/icons/font/styleguide-icons.svg',
    // la glyph : fichier contenant les correspondance caractère code = character name servant a remapper les noms de fichier
    glyph: _SRC_PATH + 'assets/fonts/icons/glyph.scss',
    destPath: _BUILD_PATH + 'fontblast/',
    cleanAfter: false
},
```




## FAQS

### Erreur sur les chemins de Sourcemaps dans les css

Lors de la création du bundle les fichiers css compilés ensembles casse les sourcemaps #11 et #14.
