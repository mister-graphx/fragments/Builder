/**  # Prototype

*/
module.exports = function(gulp, plugins, config, browserSync, onError) {
  var fsRecurs = require('fs-readdir-recursive');
  var through = require('through2');
  var util = require('util');
  var _ = require('lodash');
  var spy = require("through2-spy");
  var rename = require("gulp-rename");
  // Error Handling
  var PluginError = require('plugin-error');
  // https://nodejs.org/docs/latest/api/path.html#path_path_basename_path_ext
  var path = require('path');
  // https://nodejs.org/docs/latest/api/fs.html
  var fs = require('fs');
  // Navigation builder
  // https://www.npmjs.com/package/gulp-nav
  var nav = require('gulp-nav');
  // Markdown wrappers
  // https://www.npmjs.com/package/nunjucks-markdown
  // https://www.npmjs.com/package/gulp-marked [deprecated] cd gulp-markdown
  // On utilise marked directement
  // Une alternative envisageable https://www.npmjs.com/package/showdown
  var marked = require('marked');
  // https://www.npmjs.com/package/gulp-data
  var data = require('gulp-data');

  // @remove
  // Gulp-wrap permet d'assembler les data et les templates en appliquant au final render
  // Utilise consolidate, ce qui peut fausser la definitions des path
  // https://www.npmjs.com/package/consolidate
  // https://www.npmjs.com/package/gulp-wrap
  // var wrap = require('gulp-wrap');
  // var consolidate = require('consolidate');

  // Templating
  // Nunjuks wrapper for gulp : https://www.npmjs.com/package/gulp-nunjucks-render
  // https://www.npmjs.com/package/nunjucks
  // https://mozilla.github.io/nunjucks/
  var nunjucks = require('nunjucks');
  // Html minification
  // https://www.npmjs.com/package/gulp-htmlmin
  var htmlmin = require('gulp-htmlmin');

  // Nunjuks loader
  // Créer un loader pour que l'héritages des templates
  // soit relatif au premier niveau du template demandé
  // et non a la racine du projet
  //
  // https://mozilla.github.io/nunjucks/fr/api.html#filesystemloader
  //
  // FileSystemLoader([searchPaths], [opts])
  var loader = new nunjucks.FileSystemLoader(config.nunjuks.searchPaths,{
    noCache : true
  });
  // Environnement Configurer les options depuis la config
  // modifiable/extensible depuis le projet
  var njk = new nunjucks.Environment(loader,config.nunjuks.options);
  // Filters
  // https://www.npmjs.com/package/slug
  var slug = require("slug");

  njk.addFilter('slug', function(str,options) {
      return slug(str);
  });

  njk.addFilter('markdown', function(str,options) {

      // Traiter le markdwon avec Marked
      // Options/extensions Marked
      return marked(str, config.markedConfig, function(err,res){
          if(err){
            return new PluginError('Marked', 'Marked error in :' + file.path);
          }
          return res;
      });

  });

  const datasPath = _SRC_PATH + _DATAS_DIR;

  // @param file - path depuis ./page nom du fichier sans ext
  // @return Object
  function getDatas(filePath){
      var dataFile = datasPath + filePath + '.json';
      console.log('getDatas : dataFile :' + dataFile);
      if(fs.existsSync(dataFile))
          return JSON.parse(fs.readFileSync(dataFile));
      else
          console.log('getDatas : No file found for :' + dataFile);
  }

  return function (){
        // récupérer et stoker les settings/datas disponibles
        // ensuites dans les templates (bundle,assets, locals)
        const njkEnv = {
          bundle: getDatas(config.bundleConfig.fileName),
          app : getDatas('app'),
          locals : getDatas('locals')
        };
        // Parse all *.njk templates in _PROTOTYPES_DIR
        let stream = gulp.src(config.SrcPath + _PROTOTYPES_DIR + '*.njk')
            // Error Handler
            .pipe(plugins.plumber({
                errorHandler: onError
            }))
            .pipe(data(function(file) {
                // Env gulp Data Object
                var env = {};

                if (file.data) {
                  env = _.merge({},file.data, env);
                }

                // récupérer et stoker les settings/datas disponibles
                // ensuites dans les templates (bundle,assets, locals)
                const njkEnv = {
                    bundle: getDatas(config.bundleConfig.fileName),
                    app : getDatas('app'),
                    locals : getDatas('locals')
                };

                return env;
            }))
            // Navigation construct
            .pipe(nav())
            // Générer la page
            .pipe(through.obj(function (file, enc, cb) {

                if (file.isNull()) {
                  cb(null, file);
                  return;
                }

                if (file.isStream()) {
                  cb(new PluginError('nunjucks', 'Streaming not supported'));
                  return;
                }

                // Recupérer les gulp-data
                var env = file.data;

                return njk.render(file.path,env,function (err, res) {
            			if (err) return this.emit('error', new PluginError('nunjucks', err, { fileName: file.path }));
            			file.contents = Buffer.from(res || '');
            			this.push(file);
            			cb();
            		}.bind(this));

            	})
            )
            // .pipe(spy.obj(function(file) {
            //     console.log('-------------------------');
            //     console.log('ENV', file.data);
            // }))
            .pipe(rename({
                extname: ".html"
            }))
            .pipe(_if(function(){
                if(process.env.NODE_ENV == 'production')
                  return true;
              },htmlmin(config.htmlmin)
            ))
            // Output
            // ------
            //.pipe(gulp.dest(project.DevPath+project.cssPath))  // /!\ Attention on copie en Dev alors que Autoprefixer et combine mediaQueries ne sont pas passés
            .pipe(gulp.dest(config.BuildPath + _PROTOTYPES_DIR))
            .pipe(browserSync.stream());
        return stream;
    };
};
