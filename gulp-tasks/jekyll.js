/**  # Prototype

*/
module.exports = function(gulp, plugins, config, browserSync, onError) {
  const fsRecurs = require('fs-readdir-recursive');
  const through = require('through2');
  const util = require('util');
  const _ = require('lodash');
  const spy = require("through2-spy");
  const rename = require("gulp-rename");
  const _if = require('gulp-if');
  const log = require('fancy-log');
  // Error Handling
  const PluginError = require('plugin-error');
  // https://nodejs.org/docs/latest/api/path.html#path_path_basename_path_ext
  const path = require('path');
  // https://nodejs.org/docs/latest/api/fs.html
  const fs = require('fs');
  // Navigation builder
  // https://www.npmjs.com/package/gulp-nav
  const nav = require('gulp-nav');
  // Markdown wrappers
  // https://www.npmjs.com/package/nunjucks-markdown
  // https://www.npmjs.com/package/gulp-marked [deprecated] cd gulp-markdown
  // On utilise marked directement
  // Une alternative envisageable https://www.npmjs.com/package/showdown
  const marked = require('marked');
  // https://www.npmjs.com/package/gulp-data
  const data = require('gulp-data');

  // @remove
  // Gulp-wrap permet d'assembler les data et les templates en appliquant au final render
  // Utilise consolidate, ce qui peut fausser la definitions des path
  // https://www.npmjs.com/package/consolidate
  // https://www.npmjs.com/package/gulp-wrap
  // var wrap = require('gulp-wrap');
  // var consolidate = require('consolidate');

  // https://www.npmjs.com/package/gulp-gray-matter
  // https://www.npmjs.com/package/gray-matter#options
  // On utilise gray-matter et non le plugin gulp
  const matter = require('gray-matter');
  // Templating
  // Nunjuks wrapper for gulp : https://www.npmjs.com/package/gulp-nunjucks-render
  // https://www.npmjs.com/package/nunjucks
  // https://mozilla.github.io/nunjucks/
  const nunjucks = require('nunjucks');
  // Html minification
  // https://www.npmjs.com/package/gulp-htmlmin
  const htmlmin = require('gulp-htmlmin');

  // Nunjuks loader
  // Créer un loader pour que l'héritages des templates
  // soit relatif au premier niveau du template demandé
  // et non a la racine du projet
  //
  // https://mozilla.github.io/nunjucks/fr/api.html#filesystemloader
  //
  // FileSystemLoader([searchPaths], [opts])
  const loader = new nunjucks.FileSystemLoader(config.nunjuks.searchPaths,{
    noCache : true
  });
  // Environnement Configurer les options depuis la config
  // modifiable/extensible depuis le projet
  const njk = new nunjucks.Environment(loader,config.nunjuks.options);
  // Filters
  // https://www.npmjs.com/package/slug
  const slug = require("slug");

  njk.addFilter('slug', function(str,options) {
      return slug(str);
  });

  const datasPath = _SRC_PATH + _DATAS_DIR;
  const pagesPath = _SRC_PATH + _PAGES_DIR ;
  // @param file - path depuis ./page nom du fichier sans ext
  // @return Object
  function getDatas(filePath){
      var dataFile = datasPath + filePath + '.json';
      log('getDatas : dataFile :' + dataFile);
      if(fs.existsSync(dataFile))
          return JSON.parse(fs.readFileSync(dataFile));
      else
          log('getDatas : No file found for :' + dataFile);
  }

  return function (){
      // récupérer et stoker les settings/datas disponibles
      // ensuites dans les templates (bundle,assets, locals)
      const njkEnv = {
          bundle: getDatas(config.bundleConfig.fileName),
          app : getDatas('app'),
          locals : getDatas('locals')
      };
      njk.addGlobal('app',njkEnv.app);
        // Parse pages/**/*.md files recursively
    let stream = gulp.src(config.SrcPath + _PAGES_DIR + '**/*.md')
            // Error Handler
            .pipe(plugins.plumber({
                errorHandler: onError
            }))
            .pipe(data(function(file) {
                // Env gulp Data Object
                let env = {};

                if (file.data) {
                  env = _.merge({},file.data, env);
                }
                // Y'a t'il un fichier json de datas supplémentaires a fournir
                // extraire l'arbo relative depuis /pages
                let filePath = path.relative(pagesPath,file.path);
                // reconstruire le chemin depuis /datas
                // normalizer pour les ./file (situés à la racine de /pages)
                let dataFile = path.normalize( path.dirname(filePath) + '/' + path.basename(filePath,'.md') );
                if(fs.existsSync(datasPath + dataFile + '.json')){
                    env.datas = getDatas(dataFile);
                    log('YES : Json file found for :' + dataFile);
                }
                else {
                    env.datas = null;
                    log('No Json file found for :' + dataFile);
                }
                // récupérer et stoker dans env les njkEnv assets, locals,
                // injecter les settings/datas disponibles ensuites dans les templates
                // via {{datas.}}
                env = _.merge({},env, njkEnv);
                // On extrait et sépare entete/contenu
                // Extract/split file datas  extrac content|frontmatter
                var frontmatter = matter.read(file.path);
                // assigner tout de suite {{contents}}
                env.contents = frontmatter.content.toString();
                // Les datas du frontmatter doivent êtres accessibles
                // au premier niveau de file.data pour être correctements utilisées par gulp-wrap, gulp-nav
                // et être propagées dans l'héritage des templates
                env = _.merge({},env,frontmatter.data);
                // On traite les data avec nunjuks.renderString
                // On applique le rendu nunjuk (data , partials) au content
                // on utilise gulp-wrap pour mettre a jour file.content
                rendered = njk.renderString(env.contents, env);
                // Traiter le markdwon avec Marked
                // Options/extensions Marked
                marked(rendered, config.markedConfig, function(err,res){
                    if(err){
                      return new PluginError('Marked', 'Marked error in :' + file.path);
                    }
                    return env.contents = res;
                });
                return env;
            }))
            // Navigation construct
            .pipe(nav())
            // Générer la page
            .pipe(through.obj(function (file, enc, cb) {

                if (file.isNull()) {
                  cb(null, file);
                  return;
                }

                if (file.isStream()) {
                  cb(new PluginError('nunjucks', 'Streaming not supported'));
                  return;
                }
                // Recupérer les gulp-data
                var env = file.data;
                //  reccupérer le template défini dans l'entete du fichier md sinon page
                var templatesPath = config.nunjuks.defaultTemplatesPath,
                    ext= config.nunjuks.templateExt;
                if(fs.existsSync(templatesPath + env.layout + ext)) {
                    var template = fs.readFileSync(templatesPath + env.layout + ext).toString();
                }else{
                    var template = fs.readFileSync(templatesPath + 'page' + ext).toString();
                }

                return njk.renderString(template,env,function (err, res) {
            			if (err) return this.emit('error', new PluginError('nunjucks', err, { fileName: file.path }));
            			file.contents = Buffer.from(res || '');
            			this.push(file);
            			cb();
            		}.bind(this));

            	})
            )
            // .pipe(spy.obj(function(file) {
            //     console.log('-------------------------');
            //     console.log('ENV', file.data);
            // }))
            .pipe(rename({
                extname: ".html"
            }))
            .pipe(_if(function(){
                if(process.env.NODE_ENV == 'production')
                  return true;
              },htmlmin(config.htmlmin)
            ))
            // Output
            // ------
            .pipe(gulp.dest(config.BuildPath))
            .pipe(browserSync.stream());

        return stream;
    };
};
