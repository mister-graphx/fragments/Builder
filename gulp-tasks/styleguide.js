// 

const kss = require('kss-node');


const styleguide = function(cb){



};
gulp.task('styleguide', styleguide);
module.exports = styleguide;




module.exports = function(gulp, plugins, config, browserSync, onError) {


    return function (){
        let stream = gulp.src(config.SrcPath + _SASS_DIR + '**/*.scss')
            // Error Handler
            .pipe(plugins.plumber({
                errorHandler: onError
            }))
            .pipe(sourcemaps.init())
            // Sass
            .pipe(sass(eyeglass(config.sass)))
            .pipe(postcss([
                require("autoprefixer")(config.autoprefixer),
                require("postcss-url")(config.postcssUrl),
                require("postcss-custom-properties")
                // require("css-mqpacker")(config.mqpacker)
            ]))
            .pipe(sourcemaps.write(config.sourcemaps.path, {
      				includeContent: false,
      				sourceRoot: config.sourcemaps.sourceRoot,
      				destPath: config.bundleConfig.dest ,
      				//sourceMappingURLprefix: config.bundleConfig.dest + 'assets/css'
      			}))
            // Output
            // ------
            .pipe(gulp.dest(config.SrcPath + _CSS_DIR))
            .pipe(browserSync.stream());


        return stream;
    };
};
