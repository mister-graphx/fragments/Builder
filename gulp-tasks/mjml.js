/** # MJML

Options mjml Engine
https://github.com/mjmlio/mjml

option	           unit	       description	                    default value
fonts	             object	    Default fonts imported in the HTML rendered by HTML	See in index.js
keepComments	     boolean	  Option to keep comments in the HTML output	true
beautify	         boolean	  Option to beautify the HTML output	false
minify	           boolean	  Option to minify the HTML output	false
validationLevel	   string	    Available values for the validator: 'strict', 'soft', 'skip'	'soft'
filePath	         boolean	  Path of file, used for relative paths in mj-includes	'.'


*/
module.exports = function(gulp, plugins, config, browserSync, onError){
    const mjml = require('gulp-mjml');
    const mjmlEngine = require('mjml');
    const nunjucks = require('gulp-nunjucks');

    const log = require('fancy-log');
    const data = require('gulp-data');
    const _ = require('lodash');
    const fs = require('fs');
    const path = require('path');

    function getDatas(filePath){
        var dataFile = _SRC_PATH + filePath;
          log('getDatas : dataFile :' + dataFile);
        if(fs.existsSync(dataFile))
            return JSON.parse(fs.readFileSync(dataFile));
        else
          log('getDatas : No file found for :' + dataFile);
    }

    let stream = function(){
        // Prepare environnement
        let env = _.merge({},{
          "app": getDatas('datas/app.json')
        }, config.mjml.env);

        gulp.src(_SRC_PATH + "/emails/*.mjml")
            .pipe(plugins.plumber({
                errorHandler: onError
            }))
            .pipe(data(function(file){

              if (file.data) {
                env = _.merge({},env,file.data);
              }

              let fileName = path.basename(file.path,'.mjml');

              let dataFile = _SRC_PATH + 'emails/' + fileName + '.json';
              if(fs.existsSync(dataFile)){
                let datas = JSON.parse(fs.readFileSync(_SRC_PATH + '/emails/' + fileName + '.json'));
                env = _.merge(env,datas);
                log(env);
                return env;
              }else{
                return env;
              }
            }))
            .pipe(mjml(mjmlEngine,config.mjml.options))
            .pipe(nunjucks.compile(env))
            .pipe(gulp.dest(_BUILD_PATH + 'newsletters/'))
            .pipe(browserSync.stream());

        // Copy emails assets img, webfonts
        let _allowedFontFiles = 'svg,woff,ttf';
        let _allowedImagesFiles = 'jpg,jpeg,png,gif'
        gulp.src([_SRC_PATH + 'emails/**/*.{'+_allowedFontFiles+'}',
                  _SRC_PATH + 'emails/**/*.{'+_allowedImagesFiles+'}'
            ])
            .pipe(plugins.plumber({
                errorHandler: onError
            }))
            .pipe(gulp.dest(_BUILD_PATH + 'newsletters/'));

    }

    return stream;
};
