/* Linting: doiuse
https://github.com/anandthakker/doiuse

@state dev
*/
var postcss = require('gulp-postcss');
var doiuse = require('doiuse');
// Remove mapSource from stream
var purgeSourcemaps = require('gulp-purge-sourcemaps');


module.exports = function(gulp, plugins, config, browserSync, onError) {

    let stream = function(){

      var report = [];
      gulp.src(_SRC_PATH + 'assets/css/*.css', { cwd: process.cwd() })
          .pipe(purgeSourcemaps())
          // PostCSS
          .pipe(postcss([
            require("doiuse")({
              browsers:['ie >= 11'],
              ignore: ['rem','flexbox'], // an optional array of features to ignore
              ignoreFiles: ['**/*.css.map'], // an optional array of file globs to match against original source file path, to ignore
              onFeatureUsage: function(usageInfo) {
                  report.push(usageInfo.message+'\n');
              }
            })
          ]))
          .pipe(gulp.dest(config.BuildPath).on('end', function(){
              // console.log(report);
              const fs = require('fs');
              fs.writeFile(_SRC_PATH + _PROJECT + '-style-lint.txt', report, function(err) {
                  if(err) {
                      return console.log(err);
                  }
                  console.log("File saved!");
              });
          }));

  }
  
  return stream;

};
