/**  # Styles

- Preprocessing Sass
- Post css:
  - Autoprefixer
  - mqpacker : combine les media queries
- Sourcemaps


*/
module.exports = function(gulp, plugins, config, browserSync, onError) {
    const postcss = require('gulp-postcss');
    const sourcemaps = require('gulp-sourcemaps');
    const sass = require("gulp-sass");
    const eyeglass = require("eyeglass");

    return function (){
        let stream = gulp.src(config.SrcPath + _SASS_DIR + '**/*.scss')
            // Error Handler
            .pipe(plugins.plumber({
                errorHandler: onError
            }))
            .pipe(sourcemaps.init())
            // Sass
            .pipe(sass(eyeglass(config.sass)))
            .pipe(postcss([
                require("autoprefixer")(config.autoprefixer),
                require("postcss-url")(config.postcssUrl),
                require("postcss-custom-properties")
                // require("css-mqpacker")(config.mqpacker)
            ]))
            .pipe(sourcemaps.write(config.sourcemaps.path, {
      				includeContent: false,
      				sourceRoot: config.sourcemaps.sourceRoot,
      				destPath: config.bundleConfig.dest ,
      				//sourceMappingURLprefix: config.bundleConfig.dest + 'assets/css'
      			}))
            // Output
            // ------
            .pipe(gulp.dest(config.SrcPath + _CSS_DIR))
            .pipe(browserSync.stream());


        return stream;
    };
};
