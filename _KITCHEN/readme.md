# Kitchen

## Démarrer un nouveau projet

```bash
# Partir du boilerplate
cp -R _KITCHEN/_app-boilerplates/starter _KITCHEN/mon_projet
# Si besoin modifier la config du projet dans app.js
# Lancer la build
gulp --project=mon_projet
```


## Ajout de modules a un projet

```bash

cd _KITCHEN/mon_projet
# initialiser un pakage yarn
yarn init
# Ajouter les dépendances
yarn add fragments@https://gitlab.com/mister-graphx/fragments/framework.git

```
