/** # __APP_NAME__

*/
var prodLikeEnvs = ['production', 'staging'];

module.exports = {
  mode: 'development',
  sass:{
      outputStyle: 'nested',
      // we use eyeglass for scss module loading
      // local include path are relative to _BASE_PATH
      // eg: ../../_FRAMEWORKS
      includePaths: [],
      eyeglass:{
        useGlobalModuleCache: false,
        enableImportOnce: false,
        // specifying root lets the script run from any directory instead of having to be in the same directory.
        root: _BASE_PATH,
        // where assets are installed by eyeglass to expose them according to their output url.
        // If not provided, assets are not installed unless you provide a custom installer.
        buildDir: _BUILD_PATH,
        modules:[], // Local modules
        assets:{} // @unused
      }
  },
  sourcemaps:{
      path: ''
  },
  // Emailing - MJML
  mjml:{
    options: {
      keepComments: true,
      beautify: false,
      minify: false,
      validationLevel: 'soft',
      filePath: '.'
    }
  },
  nunjuks:{
      // templateExt: '.tpl',
  },
  bundleConfig: {
      dest: _BASE_PATH + _BUILD_DIR // Bundle destination
  },
  bundle: {
      'assets/css/main':{
          styles:[
              _SRC_PATH + _CSS_DIR + 'main.css'
          ],
          options: {
              minCss: prodLikeEnvs,
              uglify: false,
              rev: false,
              map:false,
              pluginOptions: { // pass additional options to underlying gulp plugins. By default the options object is empty
                  'gulp-clean-css': {
                      keepBreaks: true,
                      keepSpecialComments: 0
                  }
              },
              watch: {
                scripts: false, // do not watch for changes since these almost never change
                styles: true
              }
          }
      },
      'assets/js/dev.dependencies': {
        scripts: [
          './_JS_LIBS/jquery/dist/jquery.js',
          './_JS_LIBS/fixie/fixie.js',
        ],
        options: {
          useMin: prodLikeEnvs,
          uglify: false,
          rev:false,
          maps:false
        }
      },
      'assets/js/plugins': {
          scripts: [
            //'./_JS_LIBS/jquery.ui.js',
            '_EXTENSIONS/fragments/fragments/src/addons/responsive_menu/responsive_menu.js',
            './_JS_LIBS/lightbox2/src/js/lightbox.js',
            // Add libs
          ],
          options: {
            useMin: prodLikeEnvs,
            uglify: true,
            rev:false,
            maps:false
          }
      },
      'assets/js/main': {
        scripts: [
          _SRC_PATH + _JS_DIR + 'main.js',
        ],
        options: {
          useMin: prodLikeEnvs,
          uglify: false,
          rev:false,
          maps:false
        }
      }
  },
  copy: [
      {
        src: _SRC_PATH + 'assets/css/*.{map}',
        base: _SRC_PATH ,
        watch:true
      },
  ]
};
