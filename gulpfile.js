"use strict";

// Fragment Builder

// ------------------------------------------------*/
// REQUIRE
// ------------------------------------------------*/
const gulp    = require('gulp');
const log     = require('fancy-log');

const browserSync = require('browser-sync').create();
const reload      = browserSync.reload;

// Get args from command line
const argv = require('minimist')(process.argv.slice(2));

// Utils
const fs = require('fs');
const assignIn = require('lodash.assignin');
const    _ = require('lodash');

// -----------------------------------------------------------
// # Projects Configuration
// -----------------------------------------------------------
// # Project Loader
//
// Chargement du fichier de config passé en argument de la commande.
//
// `gulp task --project=project_name`
//
// @requires       minimist
// @see            https://www.npmjs.com/package/minimist
// @see            https://github.com/gulpjs/gulp/blob/master/docs/recipes/using-external-config-file.md
// @see            http://istrategylabs.com/2015/02/swept-up-in-the-stream-5-ways-isl-used-gulp-to-improve-our-process/
// Globals
global._PROJECT = argv.project ;
global._PROJECTS_PATH = "./_KITCHEN/";
global._BASE_PATH = _PROJECTS_PATH + _PROJECT + '/' ;

global._SRC_DIR = "_src/";
global._SRC_PATH = _BASE_PATH + _SRC_DIR;
global._BUILD_DIR = "_BUILD/";
global._BUILD_PATH = _BASE_PATH + _BUILD_DIR;
global._JS_DIR = "assets/js/";
global._CSS_DIR = "assets/css/";
global._IMG_DIR = "assets/images/";
global._SASS_DIR = "assets/_scss/";
global._PAGES_DIR = "pages/";
global._DATAS_DIR = "datas/";
global._TEMPLATES_DIR = "templates/";
global._PROTOTYPES_DIR = "prototypes/";

// First load .env
// https://www.npmjs.com/package/dotenv
const dotenv = require('dotenv');

if(fs.readFileSync(_BASE_PATH + '.env'))
  require('dotenv').config({path: _BASE_PATH + '/.env'});

// Project Overides
var project = require(_BASE_PATH + '/app.js');
// Default configuration + globals
var _config = require('./_config-default');

var config = _.merge({},_config, project);

// node env mode production/development
process.env.NODE_ENV = process.env.NODE_ENV || config.mode || 'production';
log.info('Starting task in mode : ' + process.env.NODE_ENV );

// Auto chargement des plugins
// @deprecated
// @todo           revenir au require et savoir ce que l'on charge quand on le charge
// @requires       gulp-load-plugin
// @see            https://www.npmjs.com/package/gulp-load-plugins
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins({
        camelize: true,
        lazy:true
    });

var $ = plugins; //shortcut

// ------------------------------------------------*/
// # SETTINGS | SHORTCUTS
// ------------------------------------------------*/
// SRC
var SrcPath = config.SrcPath; // With a ending /
// Build
var BuildPath = config.BuildPath ;// With a ending /
var JsPath = config.JsPath ;
var ImagePath = config.ImagePath ;


// Error handling, notifications
// https://www.npmjs.com/package/gulp-notify
// https://github.com/mikaelbr/node-notifier
// https://github.com/gulpjs/fancy-log
// https://github.com/chalk/chalk
var onError = function(err) {

    $.notify.onError({
        title:    "<%= error.plugin %>" ,
        subtitle: "ERROR !!",
        icon:     "./gulp-tasks/images/warning.png",
        message:  "<%= error.message %>",
        sound:    "Basso",
        wait: true
    })(err);

    $.notify.on('click', function (options) {
       return false;
    });

    this.emit('end');
};

// -----------------------------------------------------------
// # TASKS
// -----------------------------------------------------------

gulp.task('config', function(){
  return log({
    'NODE_ENV':process.env,
    'Config': config
  });
});

/* # Task Loader

[Utiliser un fichier de config externe avec Gulp](https://github.com/gulpjs/gulp/blob/master/docs/recipes/using-external-config-file.md)

@function
@name getTask
@param {task}
@see http://macr.ae/article/splitting-gulpfile-multiple-files.html

@see https://www.npmjs.com/package/gulp-task-loader
*/
function getTask(task) {
    return require('./gulp-tasks/' + task)(gulp, plugins, config, browserSync, onError);
}
// Styles
// Pre-pross/ Post : autoprefixing, …
gulp.task('styles', getTask('styles'));

gulp.task('styles:linter', getTask('styles-linter'));

// Svg Tool
gulp.task('build:sprite', getTask('svgSprite'));

gulp.task('font-blast', function(){
    var explodeFont = require('./gulp-tasks/fontblast')(config.fontblast);
    return explodeFont();
});

// ASSETS MANAGMENT
gulp.task('bundle-assets', getTask('bundle-assets'));

// TEMPLATING
gulp.task('prototype', getTask('prototype'));
gulp.task('jekyll', getTask('jekyll'));
gulp.task('newsletter', getTask('mjml'));

// IMAGES TOOLS

// var imageResize = require('gulp-image-resize');
// var pngcrush  = require('imagemin-pngcrush');
gulp.task('image-touch-icons', getTask('img_touch-icons')); // Generate touch icons
gulp.task('image-optim', getTask('img_image-optim')); // Optimize
gulp.task('image-resize', getTask('img_image-resize')); // Resize image to a max Size
gulp.task('image-responsives', getTask('img_responsives')); // Generate images variations for different brealpoints
gulp.task('image-galleries', getTask('img_galleries')); // Generate thumbs

// Utils
gulp.task('font2build', function() {
    gulp.src(_SRC_PATH + 'assets/fonts/**/*.{woff,woff2}')
        .pipe($.plumber({
                errorHandler: onError
        }))
        .pipe($.cached('fonts'))
        .pipe(gulp.dest(config.BuildPath))
        .pipe(browserSync.stream())
        .pipe($.notify({
            title: "Font files Updated",
            message: "Fon files are updated in Build Folder"
        }));
});

gulp.task('images2build', function() {
    gulp.src(config.SrcPath + config.ImagePath + '**/*.{jpg,jpeg,png,gif,svg}')
        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe($.cached('images'))
        .pipe(gulp.dest(config.BuildPath + config.ImagePath))
        .pipe(browserSync.stream())
        .pipe($.notify({
            title: "Images folder Updated",
            message: "All images files are updated in Build Folder"
        }));
});


gulp.task('assets2build', function() {
    gulp.src(config.SrcPath + '**/*.{css,js}')
        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe($.cached('assets'))
        .pipe(gulp.dest(config.BuildPath))
        .pipe(browserSync.stream())
        .pipe($.notify({
            title: "Assets Updated",
            message: "Assets files are updated in Build Folder"
        }));
});

// browser-sync starting the server.
// https://www.browsersync.io/docs/options
/*
browserSync.init({
    open: 'external',
    host: 'myproject.local',
    proxy: 'myproject.local', // or project.dev/app/
    port: 3000
});
*/
gulp.task('browser-sync', function() {
    browserSync.init(config.browserSync);
});

// ---------------------------------------------------------------------------
// # COMMANDS
// ---------------------------------------------------------------------------
var runSequence = require('run-sequence');


gulp.task('styles:watcher', function() {
    gulp.watch(config.sassPath+"**/*.scss", ['styles', function (){
          // https://github.com/dowjones/gulp-bundle-assets/blob/master/examples/full/bundle.config.js
          return gbundle.watch({
              configPath: path.join(__dirname, config.BasePath + 'app.js') ,
              results: {
                dest: config.SrcPath+'datas/',
                pathPrefix: config.bundleConfig.pathPrefix,
                fileName: config.bundleConfig.fileName
              },
              dest: config.bundleConfig.dest
          });
        }
    ]);
});

// styles-watch
var gbundle = require('gulp-bundle-assets');
var path = require('path');
gulp.task('bundle:watcher', function() {
  return function (){
    // https://github.com/dowjones/gulp-bundle-assets/blob/master/examples/full/bundle.config.js
    gbundle.watch({
        configPath: path.join(__dirname, config.BasePath + 'app.js') ,
        results: {
          dest: config.SrcPath+'datas/',
          pathPrefix: config.bundleConfig.pathPrefix,
          fileName: config.bundleConfig.fileName
        },
        dest: config.bundleConfig.dest
    });
  };
});



gulp.task('styles:watch', function (cb) {
    runSequence('styles','bundle-assets','styles:watcher','bundle:watcher',cb);
});

gulp.task('jekyll:watcher', function() {
    gulp.watch(config.SrcPath+"**/*.{njk,json,md}",['jekyll']);
});

gulp.task('jekyll:watch', function(callback) {
    runSequence('styles:watch','jekyll','browser-sync','jekyll:watcher',callback);
});
// Prototype
gulp.task('prototype:watcher', function () {
    gulp.watch(config.SrcPath+"**/*.{njk,json,md}",['prototype']);
});
gulp.task('prototype:watch', function (callback) {
    runSequence('styles:watch','prototype','browser-sync','prototype:watcher',callback);
});
// Newsletter emailing
gulp.task('newsletter:watcher', function() {
    gulp.watch(_SRC_PATH + "emails/**/*.{mjml,json}", ['newsletter']);
});
//
gulp.task('newsletter:watch', function (cb) {
    runSequence('newsletter','browser-sync','newsletter:watcher',cb);
});

gulp.task('newsletter:test', function (cb) {
  const sendmail = require('gulp-mail');
  let smtpInfo = {
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASWORD
    },
    host: process.env.SMTP_SERVER,
    secureConnection: true,
    port: 465
  };

  gulp.src(config.mjml.mailtest.emails) // Modify this to select the HTML file(s)
      .pipe(sendmail({
        to: JSON.parse(process.env.EMAIL_TO), // Array
        from: process.env.EMAIL_TO,// String
        smtp: smtpInfo
      }));
});

// ## Default Task
gulp.task('default', ['styles:watch'], function () {
    gulp.watch(config.SrcPath + config.ImagePath + "**/*.{jpg,jpeg,png,gif,svg}", ['images2build']);

});
